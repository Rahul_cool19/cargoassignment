package Assignments;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.junit.jupiter.api.Test;

class Cargotest {

	ShipingTimings st=new ShipingTimings();
	@Test
	void test() {
		Data data = new Data(LocalDateTime.of(LocalDate.of(2020,9,26), LocalTime.of(9, 0)), 10,0);
		st.get_days(data);
		assertEquals(3, data.No_of_days);
	}
	
	@Test
	void LeapYeartest() {
		Data data = new Data(LocalDateTime.of(LocalDate.of(2020,2,29), LocalTime.of(9, 0)), 10,0);
		st.get_days(data);
		assertEquals(4, data.No_of_days);
	}
	
	@Test
	void GovHolidaytest() {
		Data data = new Data(LocalDateTime.of(LocalDate.of(2020,1,1), LocalTime.of(9, 0)), 10,0);
		st.get_days(data);
		assertEquals(2, data.No_of_days);
	}
}
