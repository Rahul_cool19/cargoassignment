package Assignments;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.util.Locale;
import java.util.Scanner;

import Assignments.ShipmentDetails.Holidays;
import Assignments.ShipmentDetails.Weekends;

class ShipmentDetails {
	enum Holidays {
		JANUARY_1(LocalDate.of(2020, 1, 1)), INDEPENDENC_DAY(LocalDate.of(2020, 8, 15)),
		GANDHI_JAYANTHi(LocalDate.of(2020, 10, 2)), CHRISTMAS(LocalDate.of(2020, 12, 25)),LEAP(LocalDate.of(2020, 3, 2));
		
		LocalDate date;
		
		Holidays(LocalDate date) {
			this.date = date;
		}
		
		public int getMonth() {
			return date.getMonthValue();
		}
		
		public int getDate() {
			return date.getDayOfMonth();
		}
		
	}
	
	enum Weekends {
		SUNDAY("SUNDAY"), SATURDAY("SATURDAY");
		
		public String days;
		
		Weekends(String s) {
			this.days = s;
		}
		
		public String days() {
			return days;
		}
	}
}


public class Cargo {
	public static void main(String[] args) {
		ShipingTimings st = new ShipingTimings();
		Data data = new Data(LocalDateTime.of(LocalDate.of(2020,1,2), LocalTime.of(9, 0)), 2,10);
		st.get_days(data);
		System.out.println("the package will take " + data.get_days() + " days to reach and it will reach you by "
				+ data.gettimings()+" "+data.timeperiod());
		ZoneId fromTimeZone = ZoneId.of("Asia/Kolkata");   
        ZoneId toTimeZone = ZoneId.of("America/New_York");
        ZonedDateTime currentISTime = data.shipmentDate.atZone(fromTimeZone);
        ZonedDateTime AmericaISTime = data.shipmentDate.atZone(toTimeZone);
        System.out.println(currentISTime+" "+AmericaISTime);
	}

}

class Data {
	LocalDateTime shipmentDate,startTime;
	double No_Of_Hours = 0;
	int No_of_days = 0;
	int minutes=0;
	public Data(LocalDateTime date, int hours,int minutes) {
		this.shipmentDate = date;
		this.No_Of_Hours = hours;
		this.startTime=date;
		this.minutes=minutes;
	}
	
	public void addMinute() {
			shipmentDate=shipmentDate.plusMinutes(minutes);
			if(shipmentDate.getHour()>=18 && shipmentDate.getMinute()>0) {
				No_of_days++;
				int num = shipmentDate.getHour()-18;
				shipmentDate=shipmentDate.minusMinutes(num-1);
				shipmentDate=shipmentDate.plusHours(12);
				shipmentDate.plusMinutes(num);
			}
	}
	public int get_days() {
		if(minutes>0) {
			addMinute();
		}
		return No_of_days;
	}

	public String gettimings() {
		return shipmentDate.getHour()+ ":"+shipmentDate.getMinute();
	}
	public String timeperiod() {
		if(shipmentDate.getHour()<12) 
			return "am";
		return "pm";
	}
}

class ShipingTimings {
	boolean flag=true;
	public void get_days(Data data) {
		String days = DayOfWeek.from(data.shipmentDate).name();
		
		while (data.No_Of_Hours > 0) {
			if (data.shipmentDate.getHour() < 17 && checkHolidays(data) && checkWeekends(days) && flag) {
				flag=false;
				int num = 18 - data.shipmentDate.getHour();
				System.out.println(data.shipmentDate.getMinute());
				data.No_Of_Hours -= num;
				data.No_of_days++;
				System.out.println(data.No_Of_Hours);
				data.shipmentDate = data.shipmentDate.plusHours(num + 12);
			}
//			System.out.println(data.No_Of_Hours);
			data.No_of_days++;
			days = DayOfWeek.from(data.shipmentDate).name();
			if (!checkWeekends(days) && !checkHolidays(data)) {
				if(data.No_Of_Hours<=12) {
					data.shipmentDate = data.shipmentDate.plusHours((long) data.No_Of_Hours);
					break;
				}
				data.No_Of_Hours -= 12;
			}

			data.shipmentDate = data.shipmentDate.plusDays(1);
		}
	}

	public boolean checkWeekends(String days) {
		for (Weekends x : Weekends.values()) {

			if (x.days().equals(days)) {
				return true;
			}
		}
		return false;
	}

	public boolean checkHolidays(Data data) {
		for (Holidays x : Holidays.values()) {
			if (x.getMonth() == data.shipmentDate.getMonthValue() && x.getDate() == data.shipmentDate.getDayOfMonth()) {
				return true;
			}
		}
		return false;
	}
}
